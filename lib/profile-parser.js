/**
 * Profile parser.
 *
 * Parses profile data as fetched from Kelal Profile's API.
 *
 * @param {object|string} json
 * @return {object}
 * @access public
 */
exports.parse = function(json) {
  if ('string' === typeof json) {
    json = JSON.parse(json)
  }

  var profile = {}
    , i, len

  // _id?
  profile._id = json._id

  // name.{first?, last?}?
  if (json.name) profile.name = { first: json.name.first, last: json.name.last }

  // gender?
  profile.gender = json.gender

  // birthdate?
  profile.birthdate = json.birthdate

  // country?
  profile.country = json.country

  // emails.[{ _id, address }]?
  if (json.emails && Array.isArray(json.emails)) {
    profile.emails = []
    for (i = 0, len = json.emails.length; i < len; i++) {
      if (json.emails[i]._id || json.emails[i].address) {
        profile.emails.push({ _id: json.emails[i]._id, address: json.emails[i].address })
      }
    }
  }

  // phones.[{ _id?, code?, number? }]?
  if (json.phones && Array.isArray(json.phones)) {
    profile.phones = []
    for (i = 0, len = json.phones.length; i < len; i++) {
      if (json.phones[i]._id || json.phones[i].code || json.phones[i].number) {
        profile.phones.push({ _id: json.phones[i]._id, code: json.phones[i].code, number: json.phones[i].number })
      }
    }
  }

  // username?
  profile.username = json.username

  // done
  return profile
}
