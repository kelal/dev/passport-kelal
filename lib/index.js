// load modules
var Strategy = require('./strategy')

// expose strategy
exports = module.exports = Strategy

// exports
exports.Strategy = Strategy
